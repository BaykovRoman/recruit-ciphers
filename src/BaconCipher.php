<?php


class BaconCipher implements CiphersContract
{
    const MASK = '/[a-z]/i';

    /**
     * @param string $input
     * @return string
     */
    public function encrypt(string $input): string
    {
        $result = str_split($input);
        foreach ($result as $key => $char) {
            if (!preg_match(self::MASK, $char)) {
                continue;
            }
            $charPosInAlphabet = $this->getCharPosInAlphabet($char);
            $encodedChar = str_pad(decbin($charPosInAlphabet), 6, "A", STR_PAD_LEFT);
            $encodedChar = str_replace(['0', '1'], ['A', 'B'], $encodedChar);
            $result[$key] = $encodedChar;
        }
        return implode('', $result);
    }

    /**
     * @param string $input
     * @return string
     */
    public function decrypt(string $input): string
    {
        return preg_replace_callback(
            '/[AB]{6}/',
            function ($matches) {
                $binCode = str_replace(['A', 'B'], ['0', '1'], $matches[0]);
                $alphabetPos = bindec($binCode);
                return chr($alphabetPos + ord('A'));
            },
            $input
        );
    }

    /**
     * @return int
     */
    protected function getCharPosInAlphabet(string $char): int
    {
        return ord($char) - ord('A');
    }
}
