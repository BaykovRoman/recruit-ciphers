<?php


class AtBashCipher implements CiphersContract
{
    const MASK = '/[a-z]/i';

    public function encrypt(string $input): string
    {
        $result = str_split($input);
        foreach ($result as $key => $char) {
            if (!preg_match(self::MASK, $char)) {
                continue;
            }
            $charPosInAlphabet = $this->getCharPosInAlphabet($char);
            $newAlphabetPos = $this->getAlphabetSize() - $charPosInAlphabet;
            $result[$key] = chr($newAlphabetPos + ord('A'));
        }
        return implode('', $result);
    }

    public function decrypt(string $input): string
    {
        return $this->encrypt($input);
    }

    /**
     * @return int
     */
    protected function getAlphabetSize(): int
    {
        return ord('z') - ord('A') + 1;
    }

    /**
     * @return int
     */
    protected function getCharPosInAlphabet(string $char): int
    {
        return ord($char) - ord('A') + 1;
    }
}
