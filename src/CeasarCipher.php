<?php


class CeasarCipher implements CiphersContract
{
    public $shift = 5;
    const MASK = '/[a-z]/i';

    /**
     * @param string $input
     * @return string
     */
    public function encrypt(string $input): string
    {
        $result = str_split($input);
        foreach ($result as $key => $char) {
            if (!preg_match(self::MASK, $char)) {
                continue;
            }
            $charCode = ord($char);
            $charCode -= $this->shift;
            if ($charCode >= 91 && $charCode <= 96) {
                $charCode -= 6;
            }
            if ($charCode < ord('A')) {
                $charCode += $this->getAlphabetSize();
            }
            $result[$key] = chr($charCode);
        }
        return implode('', $result);
    }

    /**
     * @param string $input
     * @return string
     */
    public function decrypt(string $input): string
    {
        $result = str_split($input);
        foreach ($result as $key => $char) {
            if (!preg_match(self::MASK, $char)) {
                continue;
            }
            $charCode = ord($char);
            $charCode += $this->shift;
            if ($charCode >= 91 && $charCode <= 96) {
                $charCode += 6;
            }
            if ($charCode > ord('z')) {
                $charCode -= $this->getAlphabetSize();
            }
            $result[$key] = chr($charCode);
        }
        return implode('', $result);
    }

    /**
     * @return int
     */
    protected function getAlphabetSize()
    {
        return ord('z') - ord('A') + 1;
    }
}
