<?php


namespace Tests;


use BaconCipher;
use PHPUnit\Framework\TestCase;

class BaconCipherTest extends TestCase
{
    public function testEncrypt()
    {
        $cipher = new BaconCipher();
        $this->assertEquals(
            'AAAAAAAAAAABAAAABAAAAABBAAABAAAAABABAAABBAAAABBBABABBBABBAAAABBAAB BAAAAABAAAABBAAABABAAABBBAABAABAABABBAABBABAABBBBBABBBBBBAAABBBAAB',
            $cipher->encrypt('ABCDEFGHXYZ abcdefghxyz'));
        $this->assertEquals(
            '!1AAAAAAAAAAABAAAABAAAAABBAAABAAAAABABAAABBAAAABBBABABBBABBAAAABBAAB [BAAAAABAAAABBAAABABAAABBBAABAABAABABBAABBABAABBBBBABBBBBBAAABBBAAB]',
            $cipher->encrypt('!1ABCDEFGHXYZ [abcdefghxyz]'));
    }

    public function testDecrypt()
    {
        $cipher = new BaconCipher();
        $this->assertEquals('ABCDEFGHXYZ abcdefghxyz',
            $cipher->decrypt('AAAAAAAAAAABAAAABAAAAABBAAABAAAAABABAAABBAAAABBBABABBBABBAAAABBAAB BAAAAABAAAABBAAABABAAABBBAABAABAABABBAABBABAABBBBBABBBBBBAAABBBAAB'));
        $this->assertEquals('!1ABCDEFGHXYZ [abcdefghxyz]',
            $cipher->decrypt('!1AAAAAAAAAAABAAAABAAAAABBAAABAAAAABABAAABBAAAABBBABABBBABBAAAABBAAB [BAAAAABAAAABBAAABABAAABBBAABAABAABABBAABBABAABBBBBABBBBBBAAABBBAAB]'));
    }

    public function testEncDec()
    {
        $cipher = new BaconCipher();
        $this->assertEquals('ABCDEFGHXYZ abcdefghxyz', $cipher->decrypt($cipher->encrypt('ABCDEFGHXYZ abcdefghxyz')));
        $this->assertEquals('!1ABCDEFGHXYZ [abcdefghxyz]',
            $cipher->decrypt($cipher->encrypt('!1ABCDEFGHXYZ [abcdefghxyz]')));
    }
}
