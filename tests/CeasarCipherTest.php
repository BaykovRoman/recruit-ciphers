<?php


namespace Tests;


use CeasarCipher;
use PHPUnit\Framework\TestCase;

class CeasarCipherTest extends TestCase
{
    public function testEncrypt()
    {
        $cipher = new CeasarCipher();
        $this->assertEquals('vwxyzABCSTU VWXYZabcstu', $cipher->encrypt('ABCDEFGHXYZ abcdefghxyz'));
        $this->assertEquals('!1vwxyzABCSTU [VWXYZabcstu]', $cipher->encrypt('!1ABCDEFGHXYZ [abcdefghxyz]'));
    }

    public function testDecrypt()
    {
        $cipher = new CeasarCipher();
        $this->assertEquals('ABCDEFGHXYZ abcdefghxyz', $cipher->decrypt('vwxyzABCSTU VWXYZabcstu'));
        $this->assertEquals('!1ABCDEFGHXYZ [abcdefghxyz]', $cipher->decrypt('!1vwxyzABCSTU [VWXYZabcstu]'));
    }

    public function testEncDec()
    {
        $cipher = new CeasarCipher();
        $this->assertEquals('ABCDEFGHXYZ abcdefghxyz', $cipher->decrypt($cipher->encrypt('ABCDEFGHXYZ abcdefghxyz')));
        $this->assertEquals('!1ABCDEFGHXYZ [abcdefghxyz]',
            $cipher->decrypt($cipher->encrypt('!1ABCDEFGHXYZ [abcdefghxyz]')));
    }
}
