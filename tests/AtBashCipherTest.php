<?php


namespace Tests;

use AtBashCipher;
use PHPUnit\Framework\TestCase;

class AtBashCipherTest extends TestCase
{
    public function testEncrypt()
    {
        $cipher = new AtBashCipher();
        $this->assertEquals('zyxwvutscba ZYXWVUTSCBA', $cipher->encrypt('ABCDEFGHXYZ abcdefghxyz'));
        $this->assertEquals('!1zyxwvutscba [ZYXWVUTSCBA]', $cipher->encrypt('!1ABCDEFGHXYZ [abcdefghxyz]'));
    }

    public function testDecrypt()
    {
        $cipher = new AtBashCipher();
        $this->assertEquals('ABCDEFGHXYZ abcdefghxyz', $cipher->decrypt('zyxwvutscba ZYXWVUTSCBA'));
        $this->assertEquals('!1ABCDEFGHXYZ [abcdefghxyz]', $cipher->decrypt('!1zyxwvutscba [ZYXWVUTSCBA]'));
    }

    public function testEncDec()
    {
        $cipher = new AtBashCipher();
        $this->assertEquals('ABCDEFGHXYZ abcdefghxyz', $cipher->decrypt($cipher->encrypt('ABCDEFGHXYZ abcdefghxyz')));
        $this->assertEquals(
            '!1ABCDEFGHXYZ [abcdefghxyz]',
            $cipher->decrypt($cipher->encrypt('!1ABCDEFGHXYZ [abcdefghxyz]'))
        );
    }
}
